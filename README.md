# Projet 4 - Groupe 2
Armand, Gabriel, William


# Utilisation du modéle

`from joblib import load` \
`model = load('modele_baseline.joblib')` \
`model.predict(gray_scaling(im, 100).reshape(-1, 10000))` \
note : im doit etre un array numpy


# Installation de l'application en local

`1. git clone https://gitlab.com/simplon-dev-ia/grenoble-2021-2022/projets/projet-1/projet-1-groupe-2.git` \
`2. pip install -r requirements.txt` \
`3. lancer commande "python3 manage.py runserver"` 

# Utilisation de l'application deployée via le formulaire

Se rendre sur `https://face-mask-detection-simplon.herokuapp.com/` et utiliser le formulaire en ligne.


# Description des fichiers

Vous trouverez les dossiers suivants:
* "incorrect" et "correct";
contiennent les photos utilisées pour générer le dataset, placer vos photos avec port du masque correct et incorrect à cet endroit.
* "incorrect_grey_100x100" et "correct_grey_100x100" contiennent les images issue de "incorrect" et "correct" déjà traitées à titre d'exemple 
* "mask_pipeline" s'occupe des différentes étapes du pipeline;
contient les modules pour générer un dataset en JSON, détecter des visages sur une photo, mettre une image en niveaux de gris, définir des modèles et les entraîner.
* faceMaskDetection contient les fichiers nécessaires à l'application web


# Utiliser les modules de mask_pipeline

le module "dataset_creation.py" génère le fichier JSON qui sert à l'entraînement et l'évaluation du modèle
- le module "face_detection.py" réalise la détection des visages sur une image
- le module "resize_gray.py" réalise la conversion des images couleurs des dossiers "incorrect" en niveaux de gris, en créant un répertoire "incorrect_grey_100x100" et "correct_grey_100x100"
- le module "data_pipeline.py" effectue les étapes de preprocessing et génère le modèle passé en paramètre
- le module "model_pipeline.py" définit plusieurs modèles et renvoie un export du modèle en utilisant les fonctions de "data_pipeline.py"
