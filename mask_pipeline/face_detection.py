
from numpy import asarray
import cv2
import mediapipe as mp

def face_detection_for_prediction(image_for_mp):
    
    """
    La fonction prend une image, détecte l'ensemble des visages sur l'image, 
    et renvoie une liste de numpy array pour prédiction pour le modèle retenu
    """
    
    mp_face_detection = mp.solutions.face_detection
    
    np_array_list = []
    
    with mp_face_detection.FaceDetection(
        model_selection=1, min_detection_confidence=0.5) as face_detection:
        
        print("in face_detect",image_for_mp)
        image = cv2.imread(image_for_mp)
        # Convert the BGR image to RGB and process it with MediaPipe Face Detection.
        results = face_detection.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Draw face detections of each face.
        if not results.detections:
            # indiquer que mediapipe n'a pas détecté de visage
            print("no face detected")
            #annotated_image = False
            #np_array_list.append(asarray(image))
            
            return False

        else:
            
            # estèce nécessaire de renvoyer une image ?
            
            annotated_image = image.copy()
            # rajouter un enumerate
            for detection in results.detections:


                x_min = detection.location_data.relative_bounding_box.xmin
                y_min = detection.location_data.relative_bounding_box.ymin

                wid = detection.location_data.relative_bounding_box.width
                
                hei = detection.location_data.relative_bounding_box.height

                abs_x_min = int(x_min *annotated_image.shape[1])
                abs_y_min = int(y_min * annotated_image.shape[0])
                adj_wid = int(wid * annotated_image.shape[1])
                adj_hei = int(hei * annotated_image.shape[0])
                
                #adj_wid = min(int((2/3)*adj_hei)
                
                # l'image est centrée sur la bouche on remonte de manière empirique
                # la zone ne doit pas dépasser le haut de l'image
                top_left_corner = (max(abs_x_min,0),max(0,abs_y_min - int((2/3)*adj_hei)))
                
                # la zone ne doit pas dépasser le bas de l'image
                bottom_right_corner = (abs_x_min+adj_wid,min(abs_y_min+int((4/3)*adj_hei),annotated_image.shape[0]))
                
                
                np_array_list.append(annotated_image[top_left_corner[1]:bottom_right_corner[1],top_left_corner[0]:bottom_right_corner[0]])
                
                
                #cv2.rectangle(annotated_image, top_left_corner, bottom_right_corner, (0, 255, 0), 1)
                
                
                
        return np_array_list