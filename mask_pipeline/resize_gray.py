import os
import numpy as np
from PIL import Image
import cv2

# commencer par du niveau de gris
# évaluation avec Azure pour avoir un bench
# 

paths = ['../correct_grey_100x100/','../incorrect_grey_100x100/']


for path in paths:
# Check whether the specified path exists or not

    if not os.path.exists(path):
    
    # Create a new directory because it does not exist 
        os.makedirs(path)
        print("The new directory"+ path +" is created!")

    if path == '../correct_grey_100x100/':
        
        files = os.listdir("../correct/")
    else:
        files = os.listdir("../incorrect/")


    for file in files:

        size = (100,100)

        if path == '../correct_grey_100x100/':
            image = Image.open("../correct/"+file)
            
        else:
            image = Image.open("../incorrect/"+file)
        
        gray_image = image.convert('L')
        gray_image.thumbnail(size)
        gray_image.save(path+file)


def gray_scaling(rgb, new_size):

    img = Image.fromarray(rgb, 'RGB')
    gray_image = img.convert('L')
    # https://appdividend.com/2020/09/23/how-to-scale-images-in-python-using-opencv/
    gray_img_stretch = cv2.resize(np.asarray(gray_image), new_size)
    #gray_image.thumbnail(new_size)
    #np_array = np.asarray(gray_img_stretch)

    return (gray_img_stretch)
