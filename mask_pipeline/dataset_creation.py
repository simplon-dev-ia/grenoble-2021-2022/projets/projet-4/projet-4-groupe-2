import os
from PIL import Image
from numpy import asarray
import json
import os.path
import pandas as pd
from resize_gray import gray_scaling


import face_detection

multi_label_JSON = 'face_dataset.json'

#paths = ['../correct/','../incorrect/']
#paths = ['./correct_grey_100x100/','./incorrect_grey_100x100/']

def json_multi_label(filename: str, paths: list):

    """
    Crée un fichier JSON à partir des fichiers contenus dans une liste de répertoire,
    le fichier JSON contient la référence de l'image, le numpy array (non sérialisable) de l'image 
    sous la forme d'une liste et le label correspondant,
    On effectue un traitement préalable sur chaque image pour sélectionner plus précisement la zone 
    correspondant au visage
    """

    # on crée le fichier JSON s'il n'existe pas
    if not os.path.exists(filename):
        open(filename, "x")

        # on crée un dictionnaire qui sera sauvergardé en JSON
        data = {}

        # count correspond à l'id de chaque visage à travers l'ensemble des images
        count = 0

        for path in paths:
            
            # on parcourt les fichiers du dataset
            files = os.listdir(path)
            
            
            

            # pour chaque image on applique 
            for image_file in files:
                
                face_list = face_detection.face_detection_for_prediction(path + image_file)
        

                # si aucun visage n'est détecté, on passe à l'image suivante
                if not face_list:
                    
                    continue
                
                for face in face_list:
                    
                    # on conertit les 3d numpy arrays des images en couleurs en numpy 
                    # correspondant à des niveaux de gris
                    # chaque image est redimensionnée, les zones sélectionnées ont dans la plupart des cas
                    # des rations proches de 3/2

                    square_face = gray_scaling(face, (100,100))
                    


                    face_dict = dict()


                    # np array n'est pas sérialisable, on doit le convertir en liste
                    face_dict['array'] = square_face.tolist()

                    if path == '../correct/':
                        
                        # le masque correctement porté correspond au code 1
                        face_dict['masque'] = 'valide'
                        face_dict['position'] = 'menton,bouche,nez'
                    
                    # revoir les codes si on fait du multilabel
                    
                    else:
                        face_dict['masque'] = 'non valide'
                        if 'Mask_Chin' in image_file:
                            # le code 2 correspond à un masque sur le menton
                            face_dict['position'] = 'menton'
                        if 'Mask_Mouth_Chin' in image_file:
                            # le code 3 correspond à un masque sur le menton et la bouche
                            face_dict['position'] = 'menton,bouche'
                        if 'Mask_Nose_Mouth' in image_file:
                            # le code 4 correspond à un masque sur nez et la bouche mais pas le menton
                            face_dict['position'] = 'bouche,nez'

                    # chaque visage à une référence à son image d'origine
                    face_dict['file'] = image_file
                    
                    # chaque image est enregistrée avec son identifiant
                    data[count] = face_dict
                    
                    # le compteur est augmenté
                    count += 1


        with open(filename, 'w') as fp:
            
            json.dump(data, fp)


json_multi_label(multi_label_JSON, ['../correct/','../incorrect/'])

# https://sparkbyexamples.com/pandas/pandas-convert-json-to-dataframe/#:~:text=The%20json_normalize()%20function%20is,which%20returns%20a%20Pandas%20DataFrame.

