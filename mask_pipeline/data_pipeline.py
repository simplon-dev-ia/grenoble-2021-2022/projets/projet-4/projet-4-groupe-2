import os
from PIL import Image
import numpy as np
from numpy import asarray
import pandas as pd
from joblib import dump

# https://pythonmachinelearning.pro/face-recognition-with-eigenfaces/

from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn.preprocessing import RobustScaler, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.decomposition import PCA
from sklearn.neural_network import MLPClassifier

# commencer par du niveau de gris
# évaluation avec Azure pour avoir un bench

def array_extraction(paths: list):


# on peut récupérer les dimensions des lignes et des colonnes
    col = [str(i) + 'x' + str(j) for i in range(1,101) for j in range(1,101)]
    col.append('label')
    
    df = pd.DataFrame(columns=col)
    df_list=[]


    ########## partie qui exploite les images directement
    
    for path in paths:
    
    # Check whether the specified path exists or not

        dir = os.listdir(path)
        arr = np.empty((len(dir),100,100), dtype=int)
        for pos,file in enumerate(dir):
            # on adapte la taille de l'array
            image = Image.open(path+file)
            arr_image = asarray(image)
            arr[pos,:,:] = arr_image
        
    ########## fin partie qui exploite  les images   
        arr_resh = arr.reshape(-1,10000)

        

        df_arr = pd.DataFrame(arr_resh)

        #print(df_arr)

        # on rajoute le label correspondant
        if path == '../correct_grey_100x100/':
            df_arr['label'] = "valide"
        else:
            df_arr['label'] = "non valide"

        df_list.append(df_arr)

    # pour ne pas écrire de nouveau sur les mêmes index
    df = pd.concat(df_list,ignore_index=True)
    return df

def down_sampling(imbalanced_df: pd.DataFrame, class_list: list, target: str):

    """
    Détermine la classe minoritaire et rééquilibre l'ensemble du jeu de données par rapport à cette classe
    """

    # taille de la classe moins représentée
    min_size = min([len(imbalanced_df[imbalanced_df[target] == label]) for label in class_list])
    
    # équilibrage des classes
    frames = [imbalanced_df[imbalanced_df[target] == label].sample(min_size) for label in class_list]

    # nouveau dataframe
    down_samp = pd.concat(frames)

    return down_samp


def split_sample(sample_df: pd.DataFrame, target_var: str, train_test_ratio: float):

    """
    Création du jeu d'entraînement et de test à partir de l'échantillon équilibré
    """
    
    # on rajoute le stratify pour être sûr de la répartition équitable de chaque classe
    samp_X = sample_df.drop(target_var,axis=1)
    samp_y = sample_df[target_var]


    X_train, X_test, y_train, y_test = train_test_split(

        # pas besoin d'encoder la variable, mais attention à ne pas inclure la variable cible dans
        # le dataframe de X
        samp_X, 
        samp_y, 
        test_size=train_test_ratio, 
        random_state=42, 
        stratify=samp_y)

    return X_train, X_test, y_train, y_test

def pipeline_from_images(pipeline,paths):

    pred_errors=[]

    # extraction du dataset
    dataset_df = array_extraction(paths)
    print(dataset_df)

    # équilibrage des classes
    down_sampled_df = down_sampling(dataset_df, ["non valide","valide"], 'label')

    # création du jeu de test
    X_train, X_test, y_train, y_test = split_sample(down_sampled_df, 'label', 0.3)

    # Entraînement du modèle
    pipeline.fit(X_train, y_train)
    
    # récupération de la variance expliquée
    print("Variance totale expliquée par la PCA:" +  
    str(round(sum(pipeline.named_steps['pca'].explained_variance_ratio_)*100,2)) + "%" )

    y_train_pred = pipeline.predict(X_train)
    #metrics.accuracy_score(y_train, y_train_pred)
    print('étape de train')
    print(classification_report(y_train, y_train_pred))

    y_pred = pipeline.predict(X_test)

    idx=0
    for inp, prediction in zip(y_test, y_pred):
        
        if prediction != inp:
            #print(idx, 'has been classified as ', prediction, 'and should be ', inp)
            pred_errors.append((idx,inp))
        idx+=1

    print('étape de test')
    print(classification_report(y_test, y_pred))

    dump(pipeline, "modele_sans_crop")

    return pred_errors

def pipeline_from_json(pipeline):

    # création d'une liste avec les erreurs de prédiction
    pred_errors=[] 

    #print("chargement du dataset au format json")
    json_df = pd.read_json("face_dataset.json", orient ='index')

    ######################## création du dataframe #############################
    col = [str(i) + 'x' + str(j) for i in range(1,101) for j in range(1,101)]
    #col.append('label')

    df = pd.DataFrame(columns=col)

    #arr = np.empty((len(df),100,100), dtype=int)
    for pos,elt in enumerate(json_df['array']):
        arr_image = asarray(elt)
        df_line = arr_image.reshape(-1,10000).tolist()[0]

        df.loc[pos] = df_line    

    df = df.join(json_df["masque"])

    #print("équilibrage du jeu de données")
    down_sampled_df = down_sampling(df, ["valide","non valide"], 'masque')


    #print("création du jeu de test")
    # création du jeu de test
    X_train, X_test, y_train, y_test = split_sample(down_sampled_df, 'masque', 0.3)

    # Réduction des dimensions, 100 composantes
    #pca_X_train , pca_X_test= PCA_transformer(X_train, X_test, 100)

    #print("exécution du pipeline")
    # lancement de l'entraînement avec le modèle spécifié
    pipeline.fit(X_train, y_train)

    # récupération de la variance expliquée
    print("pourcentage de variance explliquée par la PCA",
    round(sum(pipeline.named_steps['pca'].explained_variance_ratio_)*100,2) )

    y_train_pred = pipeline.predict(X_train)
    #metrics.accuracy_score(y_train, y_train_pred)
    print('étape de train')
    print(classification_report(y_train, y_train_pred))

    y_pred = pipeline.predict(X_test)
    print('étape de test')
    
    idx=0
    for inp, prediction in zip(y_test, y_pred):
        if prediction != inp:
            #print(idx, 'has been classified as ', prediction, 'and should be ', inp)
            pred_errors.append((idx,inp))
        idx+=1

    print(classification_report(y_test, y_pred))

    dump(pipeline, "modele_avec_crop")

    return pred_errors


#pipeline_from_json(pipeline)
#pipeline_from_images(, paths)