from joblib import load
from data_pipeline import pipeline_from_json, pipeline_from_images
from face_detection import face_detection_for_prediction
from resize_gray import gray_scaling
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.neural_network import MLPClassifier

paths = ['../correct_grey_100x100/','../incorrect_grey_100x100/']

log_reg_pipe = Pipeline([

    ##################### fonctions de preprocessing du pipeline ##################
    
    ('standardizer', StandardScaler()),
    ('pca', PCA(n_components=100, whiten=True)),
    ####################### spécification du modèle #################################


    # le modèle binaire est compatible avec le multiclasse
    ('lrc', linear_model.LogisticRegression(
    max_iter=10, multi_class='ovr', solver='liblinear'))])

neural_pipe = Pipeline([
    
    ##################### fonctions de preprocessing du pipeline ##################
    ('standardizer2', StandardScaler()),
    ('pca', PCA(n_components=100, whiten=True)),
    ####################### spécification du modèle #################################
    # le modèle binaire est compatible avec le multiclasse
    ('mlpclf', MLPClassifier(hidden_layer_sizes=(1024,), batch_size=256, verbose=True, early_stopping=True))])


pipeline_from_images(log_reg_pipe, paths)


face_to_test = face_detection_for_prediction(
    "/home/gabriel/SIMPLONIAML/Projets/projet-4-groupe-2/crop_bench/pics_to_test/djib.jpg"
    )

test = load("modele_avec_crop")



for idx, elt in enumerate(face_to_test):
    arr = gray_scaling(elt, (100,100))
    arr = arr.reshape(-1,10000)
    res = test.predict(arr)
    print("face "+ str(idx) + str(res))