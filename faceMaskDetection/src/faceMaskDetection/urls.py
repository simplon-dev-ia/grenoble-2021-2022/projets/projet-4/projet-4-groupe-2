from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static


from detection.views import index, success, display_images

urlpatterns = [
    path('', index, name="index"),
    path('detection/', include("detection.urls")),
    path('success', success, name='success'),
    path('display_images', display_images, name='display_images'),
    path('admin/', admin.site.urls)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
