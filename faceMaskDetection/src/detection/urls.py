from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import index, success, display_images

app_name = 'detection'

urlpatterns = [
    path('', index, name="detection-index"),
    path('success', success, name='success'),
    path('display_images', display_images, name='display_images'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
