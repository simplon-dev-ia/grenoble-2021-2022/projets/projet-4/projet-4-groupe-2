from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework.decorators import action
from .models import UploadImage
from django.views.decorators.csrf import csrf_exempt
import os
from django.conf import settings
from faceMaskDetection.forms import *
from PIL import Image
from matplotlib import pyplot as plt
import cv2
import mediapipe as mp
import joblib
import json
import numpy as np


def index(request):
    if request.method == 'POST':
        form = UserImage(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # return render(request, 'image_form.html', {'form': form, 'img_obj': img_object})
            return redirect('success')
    else:
        form = UserImage()
    return render(request, 'image_form.html', {'form': form})


@csrf_exempt
@action(methods=['GET', 'POST'], detail=True)
def success(request):
    model = joblib.load("detection/modele_baseline.joblib")
    predictions = []
    # pprint.pprint(('REQUEST', request))
    if request.method == 'POST':
        im_path = os.path.join(settings.BASE_DIR, 'detection/static/images/' + request.POST.get('raw_image_name'))

        im = mediapipe_face_detection(im_path)

        # print("-------------")
        # plt.imshow(im, interpolation='nearest')
        # print("-------------")
        plt.imsave(os.path.join(settings.BASE_DIR, 'detection/static/images/' + request.POST.get('raw_image_name') + "." + request.POST.get('file_extension')), cv2.cvtColor(mediapipe_face_detection(im_path), cv2.COLOR_BGR2RGB))

        faces = face_detection_for_prediction(im_path)

        for id, face in enumerate(faces):
            # plt.imshow(face, interpolation='nearest')
            plt.imsave(os.path.join(settings.BASE_DIR,
                                    'detection/static/images/' + request.POST.get('raw_image_name') + str(
                                        id) + "." + request.POST.get('file_extension')), gray_scaling(face, 100))
            # print("face shape here|||", gray_scaling(face, 100).shape)
            predictions.append(model.predict(gray_scaling(face, 100).reshape(-1, 10000)))
        print([X for X in list(np.ravel(predictions))])
        return HttpResponse(json.dumps([X for X in list(np.ravel(predictions))]))
    print("prediction here")
    # print(list(predictions)[0])
    # for pred in predictions:
    #     print(pred)
    print("prediction here")
    return render(request, 'display_success_image.html')
    # return HttpResponse(request, 'successfully uploaded')


@csrf_exempt
@action(methods=['post'], detail=True)
def image(request):
    pass


def display_images(request):
    if request.method == 'GET':
        mask_faces = UploadImage.objects.all()
        return render((request, 'display_images.html',
                       {'faces_mask_images': mask_faces}))


def face_detection_for_prediction(image_for_mp):
    """
    La fonction prend une image, détecte l'ensemble des visages sur l'image,
    et renvoie une liste de numpy array pour prédiction pour le modèle retenu
    """

    mp_face_detection = mp.solutions.face_detection

    np_array_list = []

    with mp_face_detection.FaceDetection(
            model_selection=1, min_detection_confidence=0.5) as face_detection:

        print("in face_detect", image_for_mp)
        image = cv2.imread(image_for_mp)
        # Convert the BGR image to RGB and process it with MediaPipe Face Detection.
        results = face_detection.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Draw face detections of each face.
        if not results.detections:
            # indiquer que mediapipe n'a pas détecté de visage
            print("no face detected")
            annotated_image = False

        else:

            # estèce nécessaire de renvoyer une image ?

            annotated_image = image.copy()
            # rajouter un enumerate
            for detection in results.detections:
                x_min = detection.location_data.relative_bounding_box.xmin
                y_min = detection.location_data.relative_bounding_box.ymin

                wid = detection.location_data.relative_bounding_box.width
                hei = detection.location_data.relative_bounding_box.height

                abs_x_min = int(x_min * annotated_image.shape[1])
                abs_y_min = int(y_min * annotated_image.shape[0])
                adj_wid = int(wid * annotated_image.shape[1])
                adj_hei = int(hei * annotated_image.shape[0])

                # adj_wid = min(int((2/3)*adj_hei)

                # l'image est centrée sur la bouche on remonte de manière empirique
                # la zone ne doit pas dépasser le haut de l'image
                top_left_corner = (max(abs_x_min, 0), max(0, abs_y_min - int((2 / 3) * adj_hei)))
                print("tlc", top_left_corner)
                # la zone ne doit pas dépasser le bas de l'image
                bottom_right_corner = (
                    abs_x_min + adj_wid, min(abs_y_min + int((5 / 3) * adj_hei), annotated_image.shape[0]))
                print(bottom_right_corner)

                np_array_list.append(annotated_image[top_left_corner[1]:bottom_right_corner[1],
                                     top_left_corner[0]:bottom_right_corner[0]])

                # cv2.rectangle(annotated_image, top_left_corner, bottom_right_corner, (0, 255, 0), 1)

        return np_array_list


def mediapipe_face_detection(image_for_mp):
    model = joblib.load("detection/modele_baseline.joblib")
    mp_face_detection = mp.solutions.face_detection
    faces = face_detection_for_prediction(image_for_mp)

    with mp_face_detection.FaceDetection(
            model_selection=1, min_detection_confidence=0.5) as face_detection:

        image = cv2.imread(image_for_mp)
        # Convert the BGR image to RGB and process it with MediaPipe Face Detection.
        results = face_detection.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Draw face detections of each face.
        if not results.detections:
            # indiquer que mediapipe n'a pas détecté de visage

            annotated_image = False

        else:
            # est ce nécessaire de renvoyer une image ?
            annotated_image = image.copy()
            # rajouter un enumerate
            for id, detection in enumerate(results.detections):
                x_min = detection.location_data.relative_bounding_box.xmin
                y_min = detection.location_data.relative_bounding_box.ymin

                wid = detection.location_data.relative_bounding_box.width
                hei = detection.location_data.relative_bounding_box.height

                abs_x_min = int(x_min * annotated_image.shape[1])
                abs_y_min = int(y_min * annotated_image.shape[0])
                adj_wid = int(wid * annotated_image.shape[1])
                adj_hei = int(hei * annotated_image.shape[0])

                # l'image est centrée sur la bouche on remonte de manière empirique
                top_left_corner = (abs_x_min, abs_y_min - int((2 / 3) * adj_hei))

                # j'ai changé 5/3 pour 4/3
                bottom_right_corner = (abs_x_min + adj_wid, abs_y_min + int((4 / 3) * adj_hei))

                if model.predict(gray_scaling(faces[id], 100).reshape(-1, 10000))[0] == "non valide":
                    cv2.putText(annotated_image, model.predict(gray_scaling(faces[id], 100).reshape(-1, 10000))[0], (top_left_corner[0], top_left_corner[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
                    cv2.rectangle(annotated_image, top_left_corner, bottom_right_corner, (0, 0, 255), 10)
                else:
                    cv2.putText(annotated_image, model.predict(gray_scaling(faces[id], 100).reshape(-1, 10000))[0], (top_left_corner[0], top_left_corner[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                    cv2.rectangle(annotated_image, top_left_corner, bottom_right_corner, (0, 255, 0), 10)

        return annotated_image


def gray_scaling(rgb, new_size):
    return np.asarray(Image.fromarray(rgb, 'RGB').convert('L').resize((new_size, new_size)))


"""
def predict(request):
    arr = np.full((10000), 0.5)
    cwd = os.getcwd()
    model = joblib.load("./maskDetectionApp/model/modele_baseline.joblib")
    prediction = model.predict([arr])
    context = {"résultat": prediction}
    print("Répertoire de travail", cwd)
    return render(request, "predict.html", context=context)
"""
